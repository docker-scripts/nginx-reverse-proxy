#!/bin/bash -x

source /host/settings.sh

main() {
    setup_letsencrypt
    setup_nginx
    enable_fail2ban
}

setup_letsencrypt() {
    mkdir -p /var/www/certbot
    chown nginx: /var/www/certbot

    ln -s /etc/letsencrypt/live/ /etc/nginx/certs
}

setup_nginx() {
    # include configurations of the domains (virtual hosts)
    cat <<'EOF' > /etc/nginx/conf.d/domains.conf
include /etc/nginx/domains/*.conf;
EOF

    # disable proxy_protocol
    [[ -z $ENABLE_PROXY_PROTOCOL ]] && \
        mv /etc/nginx/conf.d/proxy_protocol.conf{,.disabled}
}

enable_fail2ban() {
    systemctl enable fail2ban
    systemctl start fail2ban
}

# start main
main "$@"
