cmd_revproxy_help() {
    cat <<_EOF
    revproxy [ ls | add | rm | ssl-cert | type | path ]
        Manage the domain '$DOMAIN' of the container.

        ls
            Show whether the domain is being managed by the reverse proxy.

        add
            Add the domain to the configuration of the reverse proxy.

        rm
            Remove the domain from the configuration of the reverse proxy.

        ssl-cert
            Tell the reverse proxy to get a letsencrypt ssl cert for the domain.

        type
            Show the type of the reverse proxy (apache2, nginx, etc.)

        path
            Show the path of the config file for the domain.

_EOF
}

cmd_revproxy() {
    local revproxydir=revproxy
    case $1 in
        ls)        ds @$revproxydir domains-ls   $DOMAIN  ;;
        add)       ds @$revproxydir domains-add  $DOMAIN $DOMAINS  ;;
        rm)        ds @$revproxydir domains-rm   $DOMAIN $DOMAINS  ;;
        ssl-cert)
                   local domain_list
                   for domain in $DOMAIN $DOMAINS; do
                       [[ $domain =~ ^(.*\.)?example\.org$ ]] && continue
                       [[ $domain =~ ^(.*\.)?example\.com$ ]] && continue
                       [[ $domain =~ \.local$ ]] && continue
                       domain_list+=" $domain"
                   done
                   [[ -n $domain_list ]] && ds @$revproxydir get-ssl-cert $domain_list
                   ;;
        type)      echo 'nginx' ;;
        path)      echo $CONTAINERS/$revproxydir/domains/$DOMAIN.conf ;;
        *)         echo -e "Usage:\\n$(cmd_revproxy_help)" ; exit ;;
    esac
}
