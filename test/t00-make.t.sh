source $(dirname $BASH_SOURCE)/common.sh

description 'Test make'

test_case 'ds make' '
    ds pull revproxy &&
    rm_container_dir &&
    ds init revproxy @revproxy &&
    cd $CONTAINERS/revproxy &&
    fix_settings &&
    ds make &&
    tail logs/nohup-*.out | grep "Successfully built" &&
    docker images --format "{{.Repository}}" | grep revproxy-test &&
    docker ps -a --format "{{.Names}}" | grep revproxy-test &&
    ds stop
'
