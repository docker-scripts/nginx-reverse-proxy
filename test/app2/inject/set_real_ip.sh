#!/bin/bash -x

ip=$(hostname -I)
network=${ip%.*}.0/16

cat <<EOF > /etc/apache2/conf-available/remoteip.conf
RemoteIPHeader X-Forwarded-For
RemoteIPTrustedProxy $network
EOF

a2enconf remoteip
a2enmod remoteip

systemctl restart apache2
