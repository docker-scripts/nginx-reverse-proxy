#!/bin/bash -x

source /host/settings.sh

# display the domain on the front page 
echo "DOMAIN=$DOMAIN" > /var/www/html/index.html

# enable HTTPS
a2ensite default-ssl
a2enmod ssl
systemctl restart apache2
