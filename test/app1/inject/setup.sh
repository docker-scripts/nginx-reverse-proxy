#!/bin/bash -x

source /host/settings.sh

# display the domain on the front page 
echo "DOMAIN=$DOMAIN" > /var/www/html/index.html

# Enable HTTPS
sed -i /etc/nginx/sites-available/default \
    -e '/443/ s/# //' \
    -e '/snakeoil/ s/# //'
systemctl reload nginx
