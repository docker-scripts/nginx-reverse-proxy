#!/bin/bash -x

ip=$(hostname -I)
network=${ip%.*}.0/16

cat <<EOF > /etc/nginx/snippets/set_real_ip.conf
real_ip_header X-Forwarded-For;
set_real_ip_from $network;
EOF

sed -i /etc/nginx/sites-available/default \
    -e '/snakeoil/ a include snippets/set_real_ip.conf;'
systemctl reload nginx
