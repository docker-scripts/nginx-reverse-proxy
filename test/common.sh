rm_container_dir() {
    rm -rf $CONTAINERS/revproxy
}

fix_settings() {
    sed -i settings.sh \
        -e '/^IMAGE=/ c IMAGE=revproxy-test' \
        -e '/^CONTAINER=/ c CONTAINER=revproxy-test' \
        -e '/^PORTS=/ c PORTS="8080:80 4043:443"'
}
