cmd_domains-rm_help() {
    cat <<_EOF
    domains-rm <domain> [<domain> ...] [-c | --ssl-cert]
         Remove one or more domains from the configuration of the web proxy.
         If the option '--ssl-cert' is given, remove the corresponding certificates as well.

_EOF
}

cmd_domains-rm() {
    local usage="Usage: $COMMAND <domain> [<domain> ...] [-c | --ssl-cert]"

    # get the options
    local ssl_cert=0
    local opts="$(getopt -o c -l ssl-cert -- "$@")"
    local err=$?
    eval set -- "$opts"
    while true; do
        case $1 in
            -c|--ssl-cert) ssl_cert=1; shift ;;
            --) shift; break ;;
        esac
    done
    [[ $err == 0 ]] || fail $usage

    # get the domains
    [[ $# -lt 1 ]] && fail $usage
    local domains="$@"

    # remove nginx config files for each domain
    for domain in $domains; do
        rm -f domains/$domain.conf
    done

    # remove any letsencrypt certificates
    if [[ $ssl_cert == 1 ]]; then
        for domain in $domains; do
            ds del-ssl-cert $domain
        done
    fi

    # reload nginx config
    ds reload
}
