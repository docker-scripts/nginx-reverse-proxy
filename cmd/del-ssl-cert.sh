cmd_del-ssl-cert_help() {
    cat <<_EOF
    del-ssl-cert <domain>
         Delete the SSL cert for the given domain.

_EOF
}

cmd_del-ssl-cert() {
    local usage="Usage: $COMMAND <domain>"

    local domain="$1"
    [[ -n $domain ]] || fail $usage

    rm -rf letsencrypt/{live,archive}/$domain
    rm -f letsencrypt/renewal/$domain.conf
    rm -f letsencrypt/{csr,keys}/*
}
