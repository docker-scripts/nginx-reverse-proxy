include(bookworm)

RUN <<EOF
  apt update && apt --yes upgrade
  apt --yes install \
      curl gnupg ssl-cert fail2ban net-tools

  # install nginx
  curl -s http://nginx.org/keys/nginx_signing.key \
       | gpg --dearmor \
       | cat > /usr/share/keyrings/nginx.gpg
  signed_by='signed-by=/usr/share/keyrings/nginx.gpg'
  repo_url='https://nginx.org/packages/mainline/debian/'
  echo "deb [$signed_by] $repo_url bookworm nginx" > /etc/apt/sources.list.d/nginx.list
  apt update
  apt install --yes nginx

  # install certbot (for getting ssl certs with letsencrypt)
  DEBIAN_FRONTEND=noninteractive \
      apt install --yes certbot
EOF