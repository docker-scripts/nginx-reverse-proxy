# bash completions for the command `ds domains-add`
_ds_domains-add() {
    local containers=$(docker ps -a --format "{{.Names}}")
    COMPREPLY=( $(compgen -W "$containers" -- $1) )
}

# bash completions for the command `ds domains-rm`
_ds_domains-rm() {
    local domains=$(ls $(_ds_container_dir)/domains/ | grep .conf | sed -e 's/.conf$//')
    COMPREPLY=( $(compgen -W "$domains" -- $1) )
}

# bash completions for the command `ds get-ssl-cert`
_ds_get-ssl-cert() {
    local domains=$(ls $(_ds_container_dir)/domains/ | grep .conf | sed -e 's/.conf$//')
    COMPREPLY=( $(compgen -W "$domains" -- $1) )
}
